### Pre-requisites
1. `pip3 install -r requirements.txt`

### Sample Data

##### gt.zip
Contains a subset of hand annotated XMLs.

##### RetNet.zip
Contains predictions from RetNet

##### EMA.zip
The output XMLs from EMA(Canada)

### Note
The xmls and zips uploaded are deleted from the data folder once the page is refreshed

##### Statistics
**NOTE** Currently, as the annotations are not done at product specific level, ignore the cans, jars etc. fields they are populated at random. 

**Info**
1. First plot represents the count of objects in the GTs and Outputs. The labels to focus are the 'product' and 'non-product' 
2. The violin plot is for the height-width distribution for GTs and Outputs. **Ignore for the time being**.

##### Foreground-Background
Here the localisation task consists of predicting only foreground objects i.e. the class label if present with the prediction is ignored. 
The classes considered for foreground currently are all the GT boxes marked as 'product'.

1. Confusion Matrix(Heatmap): Ground-truth vs Outputs.
2. Precision Recall plot using all point interpolation ([link](https://github.com/rafaelpadilla/Object-Detection-Metrics)). It also contains the point referencing to the PR value corresponding to the Confusion Matrix 
3. ROC curve

The Confidence Threshold only affects the Confusion Matrix however, IoU threshold influences all the 3 plots described above.

### How to Use

1. Execute the command `python3 src/app.py`
2. Navigate your browser to `http://127.0.0.1:5000/` [Default]
3. Upload the zips(mentioned above) by clicking on the corresponding fields on the webpage
4. Click `execute`
5. Under the Foreground-Background section, the output graphs will be charted. They are the confusion matrix, mean average precision, and (under progress) ROC
6. One can alter the values of confidence threshold and IoU threshold and the plots will change dynamically


