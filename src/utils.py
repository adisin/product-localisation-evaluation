__author__ = "Aditya Singh"
__version__ = "0.1"


import plotly.graph_objects as go
from src.xml_parser import Parser
import plotly.figure_factory as ff
import numpy as np
import operator
from sklearn.metrics import roc_curve, auc

def gen_fg_bg_plots(filemapping, fg_bg_iou_thresh, conf_thresh, allowed_types):
    z = [[0, 0], [0, 0]]
    x = ['BG', 'FG']
    y = ['BG', 'FG']
    sequences = []
    total_pos = 0
    prec = []
    rec = []
    tp = 0
    fp = 0
    for (gt_file, op_file) in filemapping:
        gt_objects = Parser.parse_objects([gt_file], only_products=True)
        op_objects = Parser.parse_objects([op_file], only_products=True)
        g_map, o_map, sequence, pos = gen_fg_bg_data(gt_objects, op_objects, iou_thresh=fg_bg_iou_thresh, conf_thresh=conf_thresh, allowed_types=allowed_types)
        total_pos += pos
        z[1][1] += len(o_map['assigned'])
        z[1][0] += len(g_map['un-assigned'])
        z[0][1] += len(o_map['un-assigned'])
        sequences.extend(sequence)
    #heatmap = go.Figure(data=go.Heatmap(z=z, x=x, y=y, hoverongaps=False))
    heatmap = go.Figure(data=ff.create_annotated_heatmap(z=z, x=x, y=y, hoverongaps=False))
    heatmap['layout']['xaxis'].update(side='bottom')
    sequences.sort(key=lambda tup: tup[0], reverse=True)

    y_test = []
    y_pred = []

    for seq in sequences:
        if seq[1] == 'tp':
            y_test.append(1)
            y_pred.append(seq[0])
            tp += 1
        else:
            y_test.append(0)
            y_pred.append(seq[0])
            fp += 1
        prec.append(tp/(tp+fp))
        rec.append(tp/total_pos)

    max_prec = 0
    for idx, val in reversed(list(enumerate(prec))):
        max_prec = max(max_prec, val)
        prec[idx] = max_prec

    mAP = 0

    for i in range(len(prec)-1):
        if prec[i] == prec[i+1]:
            mAP += (prec[i]*(rec[i+1]-rec[i]))
        else:
            mAP += (prec[i+1] * (rec[i + 1] - rec[i]))
            mAP += (0.5* (prec[i]-prec[i+1])*(rec[i + 1] - rec[i]))
    current_recall = z[1][1]/(z[1][0]+z[1][1])
    current_precision = z[1][1]/(z[0][1]+z[1][1])
    pr_fg = go.Figure(data=[go.Scatter(x=rec, y=prec, name='mAP = {0:.2f}'.format(mAP), showlegend=True)
        , go.Scatter(x=[current_recall], y=[current_precision], name='PR@IoU={0:.2f},Conf={1:.2f}'.format(fg_bg_iou_thresh, conf_thresh), showlegend=True)])

    fpr, tpr, _ = roc_curve(y_test, y_pred)
    roc_auc = auc(fpr, tpr)
    trace_roc = go.Scatter(x=fpr, y=tpr, mode='lines', line=dict(color='darkorange', width=2), name='ROC curve (area = %0.2f)' % roc_auc)
    trace_rnd = go.Scatter(x=[0, 1], y=[0, 1], mode='lines', line=dict(color='navy', width=2, dash='dash'), showlegend=False)
    layout = go.Layout(legend=dict(x=0, y=0), title='Receiver Operating Characteristic', yaxis=dict(title='TP rate', range=[-0.1, 1.2], zerolinecolor='#969696',zerolinewidth=2), xaxis=dict(title='FP rate', range=[-0.1, 1.1], zerolinecolor='#969696', zerolinewidth=2), font=dict(family="Courier New, monospace", size=12, color="#7f7f7f"))
    roc_plot = go.Figure(data=[trace_roc, trace_rnd], layout=layout)

    heatmap.update_layout(title="Confusion Matrix", xaxis_title="Output", yaxis_title="Ground-truth", font=dict(family="Courier New, monospace", size=12, color="#7f7f7f"))
    pr_fg.update_layout(legend=dict(x=0, y=0), title="Precision-Recall Curve", xaxis_title="Recall", yaxis_title="Precision", yaxis=dict(range=[-0.1, 1.1], zerolinecolor='#969696',zerolinewidth=2), xaxis=dict(range=[-0.1, 1.1], zerolinecolor='#969696', zerolinewidth=2), font=dict(family="Courier New, monospace", size=12, color="#7f7f7f"))

    return heatmap, pr_fg, roc_plot


def gen_fg_bg_data(gt_objects, op_objects, iou_thresh, conf_thresh, allowed_types):
    gt_map = {'assigned':set(), 'un-assigned':set()}
    op_map = {'assigned':set(), 'un-assigned':set()}

    op_objects.sort(key=operator.attrgetter('confidence'), reverse=True)
    total_pos = 0
    sequence = []
    for gt_idx, gt in enumerate(gt_objects):
        if gt.type in allowed_types:
            total_pos += 1

    # For Confusion matrix
    for op_idx, op in enumerate(op_objects):
        if op.type in allowed_types and op.confidence >= conf_thresh:
            for gt_idx, gt in enumerate(gt_objects):
                if gt.type in allowed_types:
                    iou = bb_intersection_over_union(op.bndbox, gt.bndbox)
                    if iou >= iou_thresh:
                        if gt_idx not in gt_map['assigned'] and op_idx not in op_map['assigned']:
                            gt_map['assigned'].add(gt_idx)
                            op_map['assigned'].add(op_idx)
                            break
    # For PR Curve
    gt_prmap = {'assigned':set(), 'un-assigned':set()}
    op_prmap = {'assigned':set(), 'un-assigned':set()}
    for op_idx, op in enumerate(op_objects):
        matched = False
        if op.type in allowed_types:
            for gt_idx, gt in enumerate(gt_objects):
                if gt.type in allowed_types:
                    iou = bb_intersection_over_union(op.bndbox, gt.bndbox)
                    if iou >= iou_thresh:
                        if gt_idx not in gt_prmap['assigned'] and op_idx not in op_prmap['assigned']:
                            gt_prmap['assigned'].add(gt_idx)
                            op_prmap['assigned'].add(op_idx)
                            matched = True
                            break
            if matched:
                sequence.append((op.confidence, 'tp'))
            else:
                sequence.append((op.confidence, 'fp'))

    for op_idx, op in enumerate(op_objects):
        #Confusion Matrix
        if op.type in allowed_types and op_idx not in op_map['assigned'] and op.confidence >= conf_thresh:
            op_map['un-assigned'].add(op_idx)
        #PR curve
        # if op.type in allowed_types and op_idx not in op_prmap['assigned']:
        #     sequence.append((op.confidence, 'fp'))

    for gt_idx, gt in enumerate(gt_objects):
        if gt.type in allowed_types and gt_idx not in gt_map['assigned']:
                gt_map['un-assigned'].add(gt_idx)

    return gt_map, op_map, sequence, total_pos


def gen_count_plot(gt_objects, op_objects):
    y_gt, y_op, x = detailed_stats(gt_objects, op_objects)

    count_plot = go.Figure(data=[
        go.Bar(name='OP', x=x, y=y_op),
        go.Bar(name='GT', x=x, y=y_gt)
    ])
    count_plot.update_layout(
        title="Count for different objects",
        yaxis_title="Count",
        font=dict(
            family="Courier New, monospace",
            size=12,
            color="#7f7f7f"
        )
    )
    count_plot.update_layout(title="Count of Objects", xaxis_title="Outputs", yaxis_title="Count", font=dict(family="Courier New, monospace", size=18, color="#7f7f7f"))
    return count_plot


def gen_height_width_plot(op_objects, gt_objects, dropdown_height_width):
    measurements = height_width_plot(op_objects, filter_products=dropdown_height_width)
    gt_measurements = height_width_plot(gt_objects, filter_products=dropdown_height_width)
    height_width_fig = go.Figure()
    obj_height = []
    obj_width = []
    labels = []

    gt_height = []
    gt_width = []
    gt_labels = []

    for measurement in dropdown_height_width:
        obj_height.extend(measurements[measurement]['height'])
        obj_width.extend(measurements[measurement]['width'])
        labels.extend([measurement for _ in range(len(measurements[measurement]['height']))])

        gt_height.extend(gt_measurements[measurement]['height'])
        gt_width.extend(gt_measurements[measurement]['width'])
        gt_labels.extend([measurement for _ in range(len(gt_measurements[measurement]['height']))])



    height_width_fig.add_trace(go.Violin(x=gt_labels,
                                         y=gt_height,
                                         line_color='red',
                                         name='gt',
                                         side='negative',
                                         legendgroup='gt',
                                         meanline_visible=True))

    height_width_fig.add_trace(go.Violin(x=gt_labels,
                                         y=gt_width,
                                         line_color='red',
                                         name='gt',
                                         legendgroup='gt',
                                         side='positive',
                                         meanline_visible=True))

    height_width_fig.add_trace(go.Violin(x=labels,
                                         y=obj_height,
                                         line_color='blue',
                                         name='height',
                                         side='negative',
                                         legendgroup='height',
                                         meanline_visible=True))

    height_width_fig.add_trace(go.Violin(x=labels,
                                         y=obj_width,
                                         line_color='green',
                                         name='width',
                                         legendgroup='width',
                                         side='positive',
                                         meanline_visible=True))


    height_width_fig.update_layout(
        title="Height-Width Distribution",
        xaxis_title="Product Types",
        yaxis_title="Pixels",
        font=dict(
            family="Courier New, monospace",
            size=12,
            color="#7f7f7f"
        )
    )
    return height_width_fig



def detailed_stats(list_of_objects_GT, list_of_objects_OP):
    per_category_count_gt = {}
    per_category_count_op = {}
    categories = set()
    y_gt = []
    y_op = []
    x = []

    for object in list_of_objects_GT:
        per_category_count_gt[object.name] = per_category_count_gt.get(object.name, 0) + 1
        categories.add(object.name)
        if object.type is not None:
            categories.add(object.type)
            per_category_count_gt[object.type] = per_category_count_gt.get(object.type, 0) + 1

    for object in list_of_objects_OP:
        per_category_count_op[object.name] = per_category_count_op.get(object.name, 0) + 1
        categories.add(object.name)
        if object.type is not None:
            categories.add(object.type)
            per_category_count_op[object.type] = per_category_count_op.get(object.type, 0) + 1

    for key in categories:
        if key in per_category_count_gt:
            y_gt.append(per_category_count_gt[key])
        else:
            y_gt.append(0)
        if key in per_category_count_op:
            y_op.append(per_category_count_op[key])
        else:
            y_op.append(0)
        x.append(key)

    return y_gt, y_op, x


def height_width_plot(list_of_objects_OP, filter_products=['box', 'can']):
    measurements = {_: {'height': [], 'width': []} for _ in filter_products}
    for object in list_of_objects_OP:
        if object.type is not None and object.type in filter_products:
            measurements[object.type]['height'].append(object.bndbox[3]-object.bndbox[1])
            measurements[object.type]['width'].append(object.bndbox[2]-object.bndbox[0])
    return measurements


def bb_intersection_over_union(boxA, boxB):
    # determine the (x, y)-coordinates of the intersection rectangle
    xA = max(boxA[0], boxB[0])
    yA = max(boxA[1], boxB[1])
    xB = min(boxA[2], boxB[2])
    yB = min(boxA[3], boxB[3])

    # compute the area of intersection rectangle
    interArea = abs(max((xB - xA, 0)) * max((yB - yA), 0))
    if interArea == 0:
        return 0
    # compute the area of both the prediction and ground-truth
    # rectangles
    boxAArea = abs((boxA[2] - boxA[0]) * (boxA[3] - boxA[1]))
    boxBArea = abs((boxB[2] - boxB[0]) * (boxB[3] - boxB[1]))

    # compute the intersection over union by taking the intersection
    # area and dividing it by the sum of prediction + ground-truth
    # areas - the interesection area
    iou = interArea / float(boxAArea + boxBArea - interArea)

    # return the intersection over union value
    return iou


def map_filenames(gt=None, op=None):
    filenames = []
    non_matched_file = None
    for gt_filename in gt:
        unmatched = True
        gt_name = (str(gt_filename).split('/')[-1]).split('.')[0]
        for op_filename in op:
            op_name = (str(op_filename).split('/')[-1]).split('.')[0]
            if op_name == gt_name:
                filenames.append((gt_filename, op_filename))
                unmatched = False
                break
        if unmatched == True:
            non_matched_file = gt_filename

    if non_matched_file is not None:
        raise AssertionError('NO matching GT found for {}'.format(non_matched_file))
    #print(filenames)
    return filenames

