__author__ = "Aditya Singh"
__version__ = "0.1"

import dash
import dash_html_components as html
import dash_core_components as dcc
from dash.dependencies import Input, Output
import os
import base64
import zipfile
from pathlib import Path
from src.xml_parser import Parser
from src.utils import gen_count_plot, gen_height_width_plot, map_filenames, gen_fg_bg_plots
import shutil

app = dash.Dash('localisation')
app.config['suppress_callback_exceptions']=True

text_style = dict(color='#444', fontFamily='sans-serif', fontWeight=300)
plotly_figure = dict(data=[dict(x=[1,2,3], y=[2,4,8])])

UPLOAD_DIRECTORY = "data"

file_names = {}

app.layout = html.Div([ 
        html.H2('Localisation Evaluation Metrics', style=text_style),
        html.P('Compress your outputs and ground-truths into a *.zip or *.gz format and upload', style=text_style),
        html.Div(className='row', children=[html.Div(id='gt_file_count', children='Your text here', style= {'width': '25%', 'display': 'inline-block'}), dcc.Upload(
        id='upload_gt',
        children=html.Div([
            'Ground-truth: Drag and Drop or ',
            html.A('Select Files'),
        ]
        ),
        style={
            'width': '25%',
            'height': '25%',
            'lineHeight': '60px',
            'borderWidth': '1px',
            'borderStyle': 'dashed',
            'borderRadius': '5px',
            'textAlign': 'center',
            'margin': '10px',
            'display': 'inline-block'
        },
        # Allow multiple files to be uploaded
        multiple=False)
        ]),
    html.Div(className='row', children=[
        html.Div(id='op_file_count', children='Your text here', style={'width': '25%', 'display': 'inline-block'}),
        dcc.Upload(
            id='upload_op',
            children=html.Div([
                'Output: Drag and Drop or ',
                html.A('Select Files'),
            ]
            ),
            style={
                'width': '25%',
                'height': '25%',
                'lineHeight': '60px',
                'borderWidth': '1px',
                'borderStyle': 'dashed',
                'borderRadius': '5px',
                'textAlign': 'center',
                'margin': '10px',
                'display': 'inline-block'
            },
            # Allow multiple files to be uploaded
            multiple=False)
        ]),
    html.Button('Evaluate', id='button', style={'width': '5%', 'margin': '1%'}),
    dcc.Slider(min=0, max=9, value=0, disabled=True),
    html.H2('Statistics', style=text_style),
    dcc.Graph(id='complete_count', figure=plotly_figure, style= {'width': '45%', 'height': '45%', 'display': 'inline-block'}),
    html.Div(className="row", children=[
        dcc.Dropdown(
            id='height-width-dropdown',
            options=[
                {'label': 'Boxes', 'value': 'box'},
                {'label': 'Cans', 'value': 'can'},
                {'label': 'Bottles', 'value': 'bottle'},
                {'label': 'Jars', 'value': 'jar'},
                {'label': 'Other_rigid', 'value': 'other_rigid'},
                {'label': 'Pusher', 'value': 'pusher'}
            ],
            style={'width': '80%', 'height': '10%', 'vertical-align': 'top', 'padding-left':'2%', 'margin-left':'5%'},
            value=['box', 'can'],
            multi=True
        ),
        dcc.Graph(id='height_width_stats', figure=plotly_figure, style={'width': '100%', 'height':'90%'})
    ], style={'width': '45%', 'height': '45%', 'display': 'inline-block'}),
    dcc.Slider(min=0, max=9, value=0, disabled=True),
    html.H2('Foreground-Background', style=text_style),
    html.Div(className="row", children=[
        html.Div(className="row", children=[html.P('IoU Threshold', style=text_style),
                                            dcc.Slider(id='fg_bg_iou_thresh', min=0.5, max=1.0, value=0.5, step=0.1, marks={0.5: '50%', 0.6:'60%', 0.7: '70%', 0.8:'80%', 0.9:'90%', 1.0:'100%'})]
                 , style={'width': '50%', 'margin-left':'25%', 'margin-top':'5%'}),
        dcc.Graph(id='heatmap_fg_bg', figure=plotly_figure, style={'width': '100%', 'height':'10%', 'margin-top':'5%', 'display': 'inline-block'})
    ], style={'width': '32%', 'display': 'inline-block'}),
    html.Div(className="row", children=[
        html.Div(className="row", children=[html.P('Confidence Threshold', style=text_style),
                                            dcc.Slider(id='fg_bg_conf_thresh', min=0.1, max=1.0, value=0.5, step=0.1, marks={0.1: '10%', 0.2: '20%', 0.3: '30%', 0.4: '40%', 0.5: '50%', 0.6:'60%', 0.7: '70%', 0.8:'80%', 0.9:'90%', 1.0:'100%'})]
                 , style={'width': '50%', 'margin-left':'25%', 'margin-top':'5%'}),
        dcc.Graph(id='pr_fg_bg', figure=plotly_figure, style={'width': '100%', 'height':'10%', 'margin-top':'5%', 'display': 'inline-block'})
    ], style={'width': '32%', 'display': 'inline-block'}),
    html.Div(className="row", children=[
        dcc.Graph(id='roc_fg_bg', figure=plotly_figure, style={'width': '100%', 'height':'100%', 'margin-top':'5%', 'display': 'inline-block'})],
        style={'width': '32%', 'height': '90%', 'display': 'inline-block'}
             ),
    #dcc.Graph(id='heatmap_fg_bg', figure=plotly_figure, style={'width': '32%', 'display': 'inline-block'}),
    dcc.Slider(min=0, max=9, value=0, disabled=True)
    ])

# @app.callback(Output('plot1', 'figure'), [Input('upload-data', 'value')] )
# def text_callback( text_input ):
#     return {'data': [dict(x=[1,2,3], y=[2,4,8], type=text_input)]}
app.css.append_css({
    'external_url': 'https://codepen.io/chriddyp/pen/bWLwgP.css'
})

def save_file(name, folder, content):
    """Decode and store a file uploaded with Plotly Dash."""

    for filename in os.listdir(folder):
        file_path = os.path.join(folder, filename)
        try:
            if os.path.isfile(file_path) or os.path.islink(file_path):
                os.unlink(file_path)
            else:
                shutil.rmtree(file_path)
        except:
            print('failed to delete {}'.format(file_path))

    data = content.encode("utf8").split(b";base64,")[1]
    with open(os.path.join(folder, name), "wb") as fp:
        fp.write(base64.decodebytes(data))

    with zipfile.ZipFile(os.path.join(folder, name), 'r') as zip_ref:
        zip_ref.extractall(os.path.join(folder, name.split('.')[0], 'xmls'))

    total_files = 0
    for filename in Path(os.path.join(folder, name.split('.')[0], 'xmls')).rglob('*.xml'):
        total_files+=1
    return os.path.join(folder, name.split('.')[0], 'xmls'), total_files


@app.callback(dash.dependencies.Output('gt_file_count', 'children'), inputs=[Input("upload_gt", "filename"), Input("upload_gt", "contents")])
def save_gt_file(uploaded_filename, uploaded_file_content):
    global file_names
    if len(file_names) >= 2:
        file_names = {}

    if uploaded_filename is not None and uploaded_file_content is not None:
        filepath, total_files = save_file(uploaded_filename, UPLOAD_DIRECTORY+'/gt/', uploaded_file_content)
        file_names['gt'] = filepath
        return 'Total files inside the compressed file: {}'.format(total_files)
    else:
        'Cant read the file!'


@app.callback(dash.dependencies.Output('op_file_count', 'children'), inputs=[Input("upload_op", "filename"), Input("upload_op", "contents")])
def save_op_file(uploaded_filename, uploaded_file_content):
    global file_names
    if len(file_names) >= 2:
        file_names = {}
    if uploaded_filename is not None and uploaded_file_content is not None:
        filepath, total_files = save_file(uploaded_filename, UPLOAD_DIRECTORY+'/op/', uploaded_file_content)
        file_names['op'] = filepath
        return 'Total files inside the compressed file: {}'.format(total_files)
    else:
        'Cant read the file!'


@app.callback(
    [Output('complete_count', 'figure'),
    Output('height_width_stats', 'figure'),
     dash.dependencies.Output('heatmap_fg_bg', 'figure'),
     dash.dependencies.Output('pr_fg_bg', 'figure'),
     dash.dependencies.Output('roc_fg_bg', 'figure')],
    [dash.dependencies.Input('button', 'n_clicks'),
     dash.dependencies.Input('height-width-dropdown', 'value'),
     dash.dependencies.Input('fg_bg_iou_thresh', 'value'),
     dash.dependencies.Input('fg_bg_conf_thresh', 'value')])
def update_output(_, dropdown_height_width, fg_bg_iou_thresh, fg_bg_conf_threshold):

    gt_files = []
    op_files = []

    for filename in Path(os.path.join(file_names['gt'])).rglob('*.xml'):
        gt_files.append(filename)

    for filename in Path(os.path.join(file_names['op'])).rglob('*.xml'):
        op_files.append(filename)

    gt_objects = Parser.parse_objects(gt_files)
    op_objects = Parser.parse_objects(op_files)

    count_plot = gen_count_plot(gt_objects, op_objects)
    height_width_fig = gen_height_width_plot(op_objects, gt_objects, dropdown_height_width)

    file_mapping = map_filenames(gt_files, op_files)
    fg_bg_heatmat, fg_bg_pr_plot, fg_bg_roc_plot = gen_fg_bg_plots(file_mapping, fg_bg_iou_thresh, conf_thresh=fg_bg_conf_threshold,
                                                                   allowed_types=['can', 'box', 'bottle', 'jar', 'other_rigid', 'pusher'])

    return count_plot, height_width_fig, fg_bg_heatmat, fg_bg_pr_plot, fg_bg_roc_plot



if __name__ == '__main__':
    app.server.run()
