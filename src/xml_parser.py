__author__ = "Aditya Singh"
__version__ = "0.1"

import xml.etree.ElementTree as ET
from pathlib import Path
import os
import numpy as np
np.random.seed(42)
OBJECT_CATEGORIES = ['can', 'box', 'bottle', 'jar', 'other_rigid', 'pusher']

class Object:
    def __init__(self, name, bndbox, confidence=1.0, type='not-product'):
        self.name = name
        self.bndbox = bndbox #(xmin, ymin, xmax, ymax)
        self.confidence = confidence
        if name == 'product':
            np.random.shuffle(OBJECT_CATEGORIES)
            self.type = OBJECT_CATEGORIES[0]
        else:
            self.type = 'not-product'

    def __str__(self):
        return '{}\n{}\n{}\n'.format(self.name, self.confidence, self.type)

class Parser:
    @staticmethod
    def parse_objects(filenames, only_products=False):

        objects = []
        for filename in filenames:
            try:
                tree = ET.parse(filename)
                root = tree.getroot()
                for element in root.findall('object'):
                    name = None
                    bndbox = [0, 0, 0, 0]
                    confidence = None
                    for attrib in element:
                        if attrib.tag == 'name':
                            name = attrib.text
                        if attrib.tag == 'bndbox':
                            for coord in attrib:
                                if coord.tag == 'xmin':
                                    bndbox[0] = float(coord.text)
                                if coord.tag == 'ymin':
                                    bndbox[1] = float(coord.text)
                                if coord.tag == 'xmax':
                                    bndbox[2] = float(coord.text)
                                if coord.tag == 'ymax':
                                    bndbox[3] = float(coord.text)
                        if attrib.tag == 'confidence':
                            confidence = float(attrib.text)
                    if confidence is None:
                        confidence = .9
                    if confidence > 1:
                        confidence = confidence/100
                    bndbox = [min(bndbox[0], bndbox[2]), min(bndbox[1], bndbox[3]), max(bndbox[0], bndbox[2]), max(bndbox[1], bndbox[3])]
                    if (only_products and name == 'product'):
                        objects.append(Object(name=name, bndbox=bndbox, confidence=confidence))
                    else:
                        if not only_products:
                            objects.append(Object(name=name, bndbox=bndbox, confidence=confidence))
            except ET.ParseError as e:
                print('{} contains the error {}'.format(filename, e.msg))
        return objects

